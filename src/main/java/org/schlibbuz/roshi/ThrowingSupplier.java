/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

/**
 * Used to implement retry logic for getHtml() and getFile() in FileReaderWeb.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
@FunctionalInterface
public interface ThrowingSupplier<T, E extends Exception> {

  /**
   * The get-method.
   * @return T the return value of the supplier-function
   */
  T get() throws E;
}
