/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.reader;

import org.jsoup.nodes.Document;

/**
 * Controls access to FileReaderWeb and FileReaderLocal.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public interface FileReader {

  /**
   * Gets a html, either from url or from path.
   * @param url the url
   * @return org.jsoup.nodes.Document
   */
  Document getHtml(String url);

  /**
   * Gets a file, either from url or from path.
   * @param url the url
   * @return String
   */
  String getFile(String url);
}
