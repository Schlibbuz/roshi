/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.reader;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.schlibbuz.roshi.ThrowingSupplier;
import org.schlibbuz.roshi.http.Http;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LoggerFactory;

/**
 * Reads files from the web.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class FileReaderWeb implements FileReader {

  private static final Log LOGGER = new LoggerFactory().getInstance(FileReaderWeb.class);

  /**
   * Default-constructor with no arguments.
   */
  public FileReaderWeb() {
  }

  /**
   * Implements the retry-logic for getHtml() and getFile().
   *
   * @param supplier the func to run
   * @param maxRetries the max number of retries
   * @return T or null
   */
  <T> T retryXTimes(ThrowingSupplier<T, IOException> supplier, final int maxRetries) {
    for (int retries = 0; retries < maxRetries; retries += 1) {
      try {
        return supplier.get();
      } catch (IOException e) {
        LOGGER.logWarn("Problem Occured While Downloading The File= " + e.getMessage());
      }
    }
    return null;
  }

  /**
   * Gets a html from the given url as a jsoup-document.
   *
   * @param url the url of the html
   * @return org.jsoup.nodes.Document
   */
  @Override
  public Document getHtml(String url) {
    return retryXTimes(() -> Jsoup.connect(url).get(), 5);
  }

  /**
   * Gets the contents of a resource, usually a css or js, form the given url.
   *
   * @param url the resource-url
   * @return String
   */
  @Override
  public String getFile(String url) {
    return retryXTimes(() -> new Http(url).get(), 5);
  }

}
