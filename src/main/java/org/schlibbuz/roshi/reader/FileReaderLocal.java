/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.reader;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LoggerFactory;

/**
 * Responsible for getting resources from the file-system.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class FileReaderLocal implements FileReader {

  private static final Log LOGGER = new LoggerFactory().getInstance(FileReaderLocal.class);

  /**
   * Default constructor with no arguments.
   */
  public FileReaderLocal() {
  }

  /**
   * Gets a html from local filesystem as jsoup doc.
   *
   * @param path the path of the resource
   * @return org.jsoup.nodes.Document
   */
  @Override
  public Document getHtml(String path) {
    try {
      return Jsoup.parse(new File(path), "UTF-8");
    } catch (IOException e) {
      LOGGER.logWarn("Problem Occured While Loading The File= " + e.getMessage());
    }
    return null;
  }

  /**
   * Gets a file from local filesystem as String.
   *
   * @param path the path of the resource
   * @return String
   */
  @Override
  public String getFile(String path) {
    try {
      return FileUtils.readFileToString(
              new File(path),
              "UTF-8"
      );
    } catch (IOException e) {
      LOGGER.logWarn("Problem Occured While Loading The File= " + e.getMessage());
    }
    return null;
  }

}
