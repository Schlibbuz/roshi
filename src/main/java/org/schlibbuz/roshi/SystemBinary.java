/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LoggerFactory;

/**
 * Stores information about system-binaries.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public final class SystemBinary {

  private static final Log LOGGER = new LoggerFactory().getInstance(SystemBinary.class);
  private static final Pattern REGEX = Pattern.compile(".*?([\\d\\.]+)");

  private final String name;

  private boolean available;
  private String version;

  /**
   * Create a class-instance and set the name of the binary.
   *
   * @param name the name of the binary
   */
  public SystemBinary(String name) {
    this.name = name;
    if (System.getProperty("os.name").equals("Windows")) {
      name += ".exe";

    }
    ProcessRunner processRunner = new ProcessRunner();
    try {
      Process process = processRunner.run(new String[]{name, "--version"});
      process.waitFor();
      // error case
      if (process.exitValue() != 0 && LOGGER.isError()) {
        available = false;
        System.out.println();
        System.out.println("Command: " + name);
        BufferedReader stdErr = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = null;
        while ((line = stdErr.readLine()) != null) {
          LOGGER.logError(line);
        }
      }

      // clear case
      available = true;
      BufferedReader stdIn = new BufferedReader(new InputStreamReader(process.getInputStream()));
      this.version = cleanVersion(stdIn.readLine());

    } catch (IOException | InterruptedException e) {
      available = false;
      LOGGER.logCritical(e.getMessage());
    }
  }

  /**
   * Extracts the version number form a version string for example "Google
   *     Chrome 78.0.3904.70" becomes "78.0.3904.70".
   *
   * @param versionString a versions string in form 'google.chrome --version'
   *     would print
   * @return String the cleaned string with only the version-number
   */
  String cleanVersion(String versionString) {
    if (versionString == null) {
      return null;
    }

    Matcher matcher = REGEX.matcher(versionString);
    if (matcher.find()) {
      return matcher.group(1);
    }
    return null;
  }

  /**
   * The name of the binary.
   *
   * @return name
   */
  public String getName() {
    return name;
  }

  /**
   * Indicates if the binary is available on your system.
   *
   * @return boolean
   */
  public boolean isAvailable() {
    return available;
  }

  /**
   * The version of the binary.
   *
   * @return String the numeric version-string
   */
  public String getVersion() {
    return version;
  }

  @Override
  public String toString() {
    return "SystemBinary{" + "name=" + name + ", version=" + version + '}';
  }

}
