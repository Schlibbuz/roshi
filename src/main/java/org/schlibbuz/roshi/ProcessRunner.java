/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.io.IOException;

/**
 * Helper-class to run system-processes.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class ProcessRunner {

  /**
   * Run a command on the system. be aware you can run anything, you
   *     are only restricted by your user-privileges.
   *
   * @param commands the command and potential arguments
   * @return Process
   * @throws IOException if an I/O error occurs
   */
  public Process run(String[] commands) throws IOException {
    Runtime rt = Runtime.getRuntime();
    return rt.exec(commands);
  }
}
