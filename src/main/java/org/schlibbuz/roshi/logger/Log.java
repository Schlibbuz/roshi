/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.logger;

/**
 * Defines methods for logging-purposes.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public interface Log {

  /**
   * Answers if the Logger is set to log criticals.
   *
   * @return boolean
   */
  public boolean isCritical();

  /**
   * Answers if the Logger is set to log errors.
   *
   * @return boolean
   */
  public boolean isError();

  /**
   * Answers if the Logger is set to log warnings.
   *
   * @return boolean
   */
  public boolean isWarn();

  /**
   * Answers if the Logger is set to log infos.
   *
   * @return boolean
   */
  public boolean isInfo();

  /**
   * Answers if the Logger is set to log verbose.
   *
   * @return boolean
   */
  public boolean isVerbose();

  /**
   * Logs criticals messages.
   *
   * @param msg the message to log
   */
  public void logCritical(Object msg);

  /**
   * Logs criticals messages.
   *
   * @param msg the message to log
   */
  public void logCritical(String msg);

  /**
   * Logs erroreous messages.
   *
   * @param msg the message to log
   */
  public void logError(Object msg);

  /**
   * Logs erroreous messages.
   *
   * @param msg the message to log
   */
  public void logError(String msg);

  /**
   * Logs warning messages.
   *
   * @param msg the message to log
   */
  public void logWarn(Object msg);

  /**
   * Logs warning messages.
   *
   * @param msg the message to log
   */
  public void logWarn(String msg);

  /**
   * Logs informative messages.
   *
   * @param msg the message to log
   */
  public void logInfo(Object msg);

  /**
   * Logs informative messages.
   *
   * @param msg the message to log
   */
  public void logInfo(String msg);

  /**
   * Logs verbose messages.
   *
   * @param msg the message to log
   */
  public void logVerbose(Object msg);

  /**
   * Logs verbose messages.
   *
   * @param msg the message to log
   */
  public void logVerbose(String msg);

  /**
   * Gets the loglevel-setting.
   *
   * @return LogLevel
   */
  public LogLevel getLogLevel();

  /**
   * Sets the loglevel.
   *
   * @param logLevel the loglevel you want to set.
   */
  public void setLogLevel(LogLevel logLevel);
}
