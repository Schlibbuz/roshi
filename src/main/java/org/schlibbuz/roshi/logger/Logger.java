/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.logger;

import java.util.Objects;

/**
 * Implementation of Logger.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public final class Logger implements Log {

  private static LogLevel logLevel = LogLevel.INFO;

  private final String name;

  /**
   * Create an instance of Logger.
   * @param name the name of the logger
   */
  public Logger(String name) {
    this.name = name;
  }

  /**
   * Get the current loglevel.
   *
   * @return String
   */
  @Override
  public LogLevel getLogLevel() {
    return logLevel;
  }

  /**
   * Sets the loglevel, affects all loggers.
   *
   * @param logLevel the loglevel to set
   */
  @Override
  public void setLogLevel(LogLevel logLevel) {
    Logger.logLevel = logLevel;
  }

  /**
   * Asks if current loglevel is set to critical.
   *
   * @return boolean
   */
  @Override
  public boolean isCritical() {
    return logLevel.isAtLeast(LogLevel.CRITICAL);
  }

  /**
   * Asks if current loglevel is set to error.
   *
   * @return boolean
   */
  @Override
  public boolean isError() {
    return logLevel.isAtLeast(LogLevel.ERROR);
  }

  /**
   * Asks if current loglevel is set to warn.
   *
   * @return boolean
   */
  @Override
  public boolean isWarn() {
    return logLevel.isAtLeast(LogLevel.WARNINGS);
  }

  /**
   * Asks if current loglevel is set to info.
   *
   * @return boolean
   */
  @Override
  public boolean isInfo() {
    return logLevel.isAtLeast(LogLevel.INFO);
  }

  /**
   * Asks if current loglevel is set to verbose.
   *
   * @return boolean
   */
  @Override
  public boolean isVerbose() {
    return logLevel.isAtLeast(LogLevel.VERBOSE);
  }

  /**
   * Log a critcal.
   * @param msg the message to log
   */
  @Override
  public void logCritical(Object msg) {
    logCritical(String.valueOf(msg));
  }

  /**
   * Log a critcal.
   * @param msg the message to log
   */
  @Override
  public void logCritical(String msg) {
    System.out.println(msg);
  }

  /**
   * Log an error.
   * @param msg the message to log
   */
  @Override
  public void logError(Object msg) {
    logError(String.valueOf(msg));
  }

  /**
   * Log an error.
   * @param msg the message to log
   */
  @Override
  public void logError(String msg) {
    if (Logger.logLevel.isAtLeast(LogLevel.ERROR)) {
      System.out.println(msg);
    }
  }

  /**
   * Log a warning.
   * @param msg the message to log
   */
  @Override
  public void logWarn(Object msg) {
    logWarn(String.valueOf(msg));
  }

  /**
   * Log a warning.
   * @param msg the message to log
   */
  @Override
  public void logWarn(String msg) {
    if (Logger.logLevel.isAtLeast(LogLevel.WARNINGS)) {
      System.out.println(msg);
    }
  }

  /**
   * Log an info.
   * @param msg the message to log
   */
  @Override
  public void logInfo(Object msg) {
    logInfo(String.valueOf(msg));
  }

  /**
   * Log an info.
   * @param msg the message to log
   */
  @Override
  public void logInfo(String msg) {
    if (Logger.logLevel.isAtLeast(LogLevel.INFO)) {
      System.out.println(msg);
    }
  }

  /**
   * Log a verbose.
   * @param msg the message to log
   */
  @Override
  public void logVerbose(Object msg) {
    logVerbose(String.valueOf(msg));
  }

  /**
   * Log a verbose.
   * @param msg the message to log
   */
  @Override
  public void logVerbose(String msg) {
    if (Logger.logLevel.isAtLeast(LogLevel.VERBOSE)) {
      System.out.println(msg);
    }
  }

  /**
   * The hash-code-function.
   *
   * @return int
   */
  @Override
  public int hashCode() {
    int hash = 5;
    hash = 73 * hash + Objects.hashCode(this.name);
    return hash;
  }

  /**
   * Compares one logger to another.
   *
   * @param obj the logger to compare
   * @return boolean
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Logger other = (Logger) obj;
    return Objects.equals(this.name, other.name);
  }

}
