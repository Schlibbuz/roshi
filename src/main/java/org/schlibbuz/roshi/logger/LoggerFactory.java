/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Class LoggerFactory, produces Logger-Instances.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class LoggerFactory {

  /**
   * Holds references for different class-loggers.
   */
  private static final Map<String, Log> INSTANCES = new HashMap<>();

  /**
   * Default-constructor with no arguments.
   */
  public LoggerFactory() {
  }

  /**
   * Helper-method, redirects to getInstance(String name).
   *
   * @param clazz the class to get the instance for.
   * @return Log
   */
  public Log getInstance(Class clazz) {
    return getInstance(clazz.getName());
  }

  /**
   * Get the instance of logger for the given class, if it does not exist create
   *     one.
   *
   * @param name the name of the class to get the instance for.
   * @return Log
   */
  public Log getInstance(String name) {
    Log instance = INSTANCES.get(name);
    if (instance == null) {
      instance = new Logger(name);
      INSTANCES.put(name, instance);
    }
    return instance;
  }

}
