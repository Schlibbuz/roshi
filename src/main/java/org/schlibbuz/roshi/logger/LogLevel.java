/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.logger;

/**
 * Defines log-levels.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public enum LogLevel {
  CRITICAL(0),
  ERROR(1),
  WARNINGS(2),
  INFO(3),
  VERBOSE(4);

  private final int severity;

  /**
   * Constructor.
   * @param severity the log severity to set
   */
  LogLevel(int severity) {
    this.severity = severity;
  }

  /**
   * Checks if log-level is at least severity.
   *
   * @param other the loglevel to compare
   * @return boolean
   */
  public boolean isAtLeast(LogLevel other) {
    return this.severity >= other.severity;
  }
}
