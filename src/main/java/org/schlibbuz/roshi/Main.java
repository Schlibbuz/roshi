/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.io.IOException;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LogLevel;
import org.schlibbuz.roshi.logger.LoggerFactory;

/**
 * Class Main.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public final class Main {

  private static final Log LOGGER = new LoggerFactory().getInstance(Main.class);
  private static Env env;

  /**
   * The main-method.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    LOGGER.setLogLevel(LogLevel.INFO);
    try {
      env = new PrerequesiteChecker().checkSys();
      Analyzer analyzer = new Analyzer("data/test/2/index.html", env, true);
      analyzer.extractJsData();
      analyzer.analyzeBody();
    } catch (IOException e) {
      LOGGER.logCritical(e.getMessage());
      LOGGER.logInfo("cannot proceed, cya");
    }
  }

}
