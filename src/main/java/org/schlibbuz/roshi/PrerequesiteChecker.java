/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.io.IOException;
import org.schlibbuz.roshi.feature.Feature;
import org.schlibbuz.roshi.feature.FeatureFactory;
import org.schlibbuz.roshi.feature.UnsupportedFeatureException;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LoggerFactory;

/**
 * Checks prerequesties the application requires to run.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public final class PrerequesiteChecker {

  private static final Log LOGGER = new LoggerFactory().getInstance(Main.class);

  private final Env env;

  /**
   * Sets up Env.
   */
  public PrerequesiteChecker() {
    env = new Env();
  }

  /**
   * Checks the system for features and collects information about them.
   *
   * @return the Env with features
   * @throws IOException when an unsupported feature is requested.
   */
  public Env checkSys() throws IOException {
    checkSysDeps();
    return env;
  }

  /**
   * Checks system for tools to run containerization, selenium-tests and js-linting.
   *
   * @throws java.io.IOException when an unsupported feature is requested
   */
  void checkSysDeps() throws IOException {
    LOGGER.logInfo("checking prerequesites");
    System.out.println("____________________________\n");
    checkOs();
    checkFeature("node");
    checkFeature("selenium");
    checkFeature("containerization");
    System.out.println("____________________________");
    System.out.println("############################");
  }

  /**
   * Checks which os is running and saves it in Env.
   */
  void checkOs() {
    LOGGER.logInfo("OS is?          -> " + env.getOs());
  }

  /**
   * Checks for a feature and adds it to env if its available.
   *
   * @param name the name of the feature. at the moment you can check for
   *     node, selenium and containerization.
   */
  void checkFeature(String name) {
    try {
      Feature feature = FeatureFactory.produce(name, env).find();
      LOGGER.logInfo(feature);
      env.addFeature(feature);
    } catch (UnsupportedFeatureException e) {
      System.err.println(e.getMessage());
    }
  }

  /**
   * Getter for Env.
   *
   * @return Env
   */
  public Env getEnv() {
    return env;
  }

}
