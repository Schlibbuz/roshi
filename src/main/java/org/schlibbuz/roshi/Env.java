/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.util.HashMap;
import java.util.Map;
import org.schlibbuz.roshi.feature.Feature;

/**
 * Holds information of what the host is supporting.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class Env {

  private final Map<String, Feature> features;
  private final Map<String, SystemBinary> systemBinaries;
  private final String os;

  /**
   * Default constructor.
   */
  public Env() {
    features = new HashMap<>();
    systemBinaries = new HashMap<>();
    os = System.getProperty("os.name");
  }

  /**
   * Gets the os-type.
   *
   * @return the os as String
   */
  public String getOs() {
    return os;
  }

  /**
   * Gets a system-feature.
   *
   * @param name the name of the feature.
   * @return Feature or null
   */
  public Feature getFeature(String name) {
    return features.get(name);
  }

  /**
   * Adds a system-feature.
   *
   * @param feature the feature to be added
   */
  public void addFeature(Feature feature) {
    features.put(feature.getName(), feature);
  }

  /**
   * Gets a system-binary.
   *
   * @param name the name of the binary.
   * @return SystemBinary or null
   */
  public SystemBinary getSystemBinary(String name) {
    return systemBinaries.get(name);
  }

  /**
   * Adds a system-binary.
   *
   * @param systemBinary the system-binary to be added
   */
  public void addSystemBinary(SystemBinary systemBinary) {
    systemBinaries.put(systemBinary.getName(), systemBinary);
  }

  /**
   * Checks if a feature is supported or not.
   *
   * @param feature the name of the feature.
   * @return boolean
   */
  public boolean supports(String feature) {
    return features.get(feature) != null;
  }

  /**
   * String representation.
   * @return
   */
  @Override
  public String toString() {
    return "Env{" + "features=" + features + ", systemBinaries=" + systemBinaries + ", os=" + os + '}';
  }

}
