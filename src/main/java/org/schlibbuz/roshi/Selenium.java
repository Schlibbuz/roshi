/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LoggerFactory;
import org.schlibbuz.roshi.reader.FileReader;
import org.schlibbuz.roshi.reader.FileReaderWeb;

/**
 * Provides a selenium-instance to scrape responsive websites.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class Selenium extends FileReaderWeb implements FileReader {

  private static final Log LOGGER = new LoggerFactory().getInstance(Selenium.class);

  /**
   * Gets a html form a given url.
   *
   * @param url the resource
   * @return org.jsoup.nodes.Document
   */
  @Override
  public Document getHtml(String url) {
    ChromeOptions chromeOpts = new ChromeOptions();
    chromeOpts.addArguments("--headless");
    WebDriver browser = new ChromeDriver(chromeOpts);
    browser.get("http://localhost:3000");

    LOGGER.logVerbose(browser.getPageSource());
    List<WebElement> regions = browser.findElements(By.className("js-region-link"));
    LOGGER.logVerbose(String.valueOf(regions.size()));

    for (WebElement webElement : regions) {
      System.out.println(webElement.getText());
    }
    WebElement spilistan = regions.get(1);
    spilistan.click();
    try {
      Thread.currentThread().sleep(5000);
    } catch (InterruptedException e) {
      LOGGER.logWarn(e.getMessage());
    }
    browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    List<WebElement> courses = browser.findElements(By.className("course-compact"));

    LOGGER.logVerbose(String.valueOf(courses.size()));

    if (LOGGER.isVerbose()) {
      for (WebElement course : courses) {
        LOGGER.logVerbose(course.getText());
      }
    }

    System.out.println();
    LOGGER.logVerbose(String.valueOf(courses.size()));

    Navigation nav = browser.navigate();
    nav.to("http://localhost:3000/calendar");
    return null;
  }

  /**
   * Gets a resource, usually css or js, from a given url.
   *
   * @param url the resource
   * @return String the contents of the resource
   */
  @Override
  public String getFile(String url) {
    return super.getFile(url);
  }
}
