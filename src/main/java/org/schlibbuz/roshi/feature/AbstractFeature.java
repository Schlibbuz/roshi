/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import java.util.HashMap;
import java.util.Map;
import org.schlibbuz.roshi.Env;
import org.schlibbuz.roshi.SystemBinary;

/**
 * Holds information if a feature is supported on the system.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class AbstractFeature implements Feature {

  /**
   * The name of the feature.
   */
  final String name;
  /**
   * The system-binaries required for the feature to work.
   */
  final Map<String, SystemBinary> systemBinaries;
  final Env env;

  /**
   * Create an instance of SystemFeature.
   *
   * @param name the name of the feature
   */
  AbstractFeature(String name, Env env) {
    this.name = name;
    systemBinaries = new HashMap<>();
    this.env = env;
  }

  /**
   * Get the name of the feature.
   *
   * @return String
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * Get the list of system-binaries required for this feature.
   *
   * @return Map
   */
  Map<String, SystemBinary> getSystemBinaries() {
    return systemBinaries;
  }

  // most simple case, feature has the same name as binary, 1 binary makes the feature available.
  @Override
  public Feature find() {
    SystemBinary systemBinary = new SystemBinary(name);
    if (systemBinary.isAvailable()) {
      systemBinaries.put(name, systemBinary);
    }
    return this;
  }
}
