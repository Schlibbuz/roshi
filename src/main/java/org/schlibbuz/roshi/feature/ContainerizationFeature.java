/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import org.schlibbuz.roshi.Env;

/**
 * Model of a containerization-feature, either docker or podman.
 * @author stefan
 */
public class ContainerizationFeature extends AbstractFeature implements Feature {

  /**
   * Default constructor with no arguments.
   */
  public ContainerizationFeature(Env env) {
    super("containerization", env);
  }

  /**
   * String representation.
   * @return String
   */
  @Override
  public String toString() {
    return "ContainerizationFeature{" + "name=" + name + ", systemBinaries=" + systemBinaries + '}';
  }

}
