/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

/**
 * Exception for unsupported features.
 * @author stefan.a.frei@gmail.com
 */
public class UnsupportedFeatureException extends Exception {

  /**
   * Creates a new instance of <code>UnsupportedFeatureException</code> without
   *     detail message.
   */
  public UnsupportedFeatureException() {
  }

  /**
   * Constructs an instance of <code>UnsupportedFeatureException</code> with the
   *     specified detail message.
   *
   * @param msg the detail message.
   */
  public UnsupportedFeatureException(String msg) {
    super(msg);
  }
}
