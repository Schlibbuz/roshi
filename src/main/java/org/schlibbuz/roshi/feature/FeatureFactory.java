/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import org.schlibbuz.roshi.Env;

/**
 * Factory to create different types of features.
 * @author stefan.a.frei@gmail.com
 */
public class FeatureFactory {

  /**
   * Produces a feature with the given name.
   * @param name the feature you want
   * @return implementation of org.schlibbuz.roshi.feature.AbstractFeature
   * @throws UnsupportedFeatureException if feature is not supported
   */
  public static Feature produce(String name, Env env) throws UnsupportedFeatureException {
    name = name.toLowerCase();
    if (name.equals("node")) {
      return new NodeFeature(env);
    }
    if (name.equals("containerization")) {
      return new ContainerizationFeature(env);
    }
    if (name.equals("selenium")) {
      return new SeleniumFeature(env);
    }
    throw new UnsupportedFeatureException("feature not available");
  }

}
