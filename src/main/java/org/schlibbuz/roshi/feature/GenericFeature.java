/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import org.schlibbuz.roshi.Env;

/**
 * Represents a generic feature.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class GenericFeature extends AbstractFeature implements Feature {

  /**
   * Instantiates a
   * n Object of type GenericFeature.
   * @param name the name of the feature
   */
  GenericFeature(String name, Env env) {
    super(name, env);
  }

}
