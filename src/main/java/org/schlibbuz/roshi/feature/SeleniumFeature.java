/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import java.net.MalformedURLException;
import org.schlibbuz.roshi.Env;
import org.schlibbuz.roshi.SystemBinary;
import org.schlibbuz.roshi.http.Http;

/**
 * Selenium-Feature. Used to scrape responsive webpages.
 * @author stefan
 */
public class SeleniumFeature extends AbstractFeature implements Feature {

  private static final String CHROMIU_DRIVER_API_PREFIX =
      "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_";

  /**
   * Create an instance of a SeleniumFeature.
   */
  SeleniumFeature(Env env) {
    super("selenium", env);
  }

  /**
   * String representation.
   * @return String
   */
  @Override
  public String toString() {
    return "SeleniumFeature{" + "name=" + name + ", systemBinaries=" + systemBinaries + '}';
  }

  /**
   * Provides information about a feature and the system binaries required to use it.
   * @return Feature
   */
  @Override
  public Feature find() {
    // prefer chromium
    SystemBinary chromedriver = new SystemBinary("chromedriver");
    return this;
  }

  /**
   * Verifies version and driver are compatible.
   * https://chromedriver.chromium.org/downloads/version-selection
   *
   * @return boolean
   */
  boolean verifyCompatibility() {
    SystemBinary driver = systemBinaries.get("chromedriver");
    if (driver == null) {
      return false;
    }
    SystemBinary chromium = systemBinaries.get("chromium-browser");
    if (chromium == null) {
      return false;
    }
    return driver.getVersion().equals(
        requestMatchingDriverVersion(
            composeChromiumDriverApiUrl(
                chromium.getVersion()
            )
        )
    );
  }

  /**
   * Gets the versionnumber compatible with chromium driver-api to determine
   *     which chromedriver to use.
   * @param browserVersion - the browser version-string
   * @return String
   */
  String getBrowserVersionToQuery(String browserVersion) {
    return browserVersion.substring(0, browserVersion.lastIndexOf("."));
  }

  /**
   * Determines which chromedriver to use for a specific chrome/chromium version.
   * @param url - the chromium-driver-api-url
   * @return String or null
   */
  String requestMatchingDriverVersion(String url) {
    try {
      return new Http(url).get();
    } catch (MalformedURLException e) {
      System.out.println(e.getMessage());
    }
    return null;
  }

  /**
   * Compose an url to query the matching chromedriver from chromium-driver-api.
   * @param chromeVersion the version number of chrome or chromium
   * @return String
   */
  String composeChromiumDriverApiUrl(String chromeVersion) {
    return CHROMIU_DRIVER_API_PREFIX + getBrowserVersionToQuery(chromeVersion);
  }

}
