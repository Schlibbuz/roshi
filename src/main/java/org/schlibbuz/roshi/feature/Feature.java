/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

/**
 * Interface Feature.
 * @author stefan
 */
public interface Feature {

  /**
   * Find a feature.
   * @return Feature
   */
  public Feature find();

  /**
   * Get the name of a feature.
   * @return String
   */
  public String getName();
}
