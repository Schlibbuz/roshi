/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import org.schlibbuz.roshi.Env;

/**
 * Node-Feature. Used for linting js-code.
 * @author stefan
 */
public class NodeFeature extends AbstractFeature implements Feature {

  /**
   * Create an instance of a NodeFeature.
   */
  NodeFeature(Env env) {
    super("node", env);
  }

  /**
   * String representation.
   * @return
   */
  @Override
  public String toString() {
    return "NodeFeature{" + "name=" + name + ", systemBinaries=" + systemBinaries + '}';
  }
}
