/**
 * Provides functionbality to analyze a website.
 *
 * @since 1.0
 * @author stefan.a.frei@gmail.com
 */

package org.schlibbuz.roshi;
