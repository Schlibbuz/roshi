/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LoggerFactory;
import org.schlibbuz.roshi.reader.FileReader;
import org.schlibbuz.roshi.reader.FileReaderLocal;
import org.schlibbuz.roshi.reader.FileReaderWeb;

/**
 * Analyzes a website including js and css files.
 *
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public final class Analyzer {

  private static final Log LOGGER = new LoggerFactory().getInstance(Analyzer.class);

  private final String url;
  private final FileReader fileReader;
  private final List<String> jsFrags;
  private final Map<String, Integer> tagStats;
  private final Map<String, Integer> cssStats;
  private final Env env;

  private String host;
  private final Document doc;

  /**
   * Constructor.
   * @param url the entry-point html
   * @param env indicates which analyzing-processes are supported by your system.
   * @throws MalformedURLException when the url is malformed.
   */
  public Analyzer(String url, Env env) throws MalformedURLException {
    fileReader = new FileReaderWeb();
    URL parsedUrl = new URL(url);
    this.host = parsedUrl.getProtocol() + "://" + parsedUrl.getAuthority();
    this.url = parsedUrl.toString();
    this.env = env;
    jsFrags = new ArrayList<>();
    tagStats = new HashMap<>();
    cssStats = new TreeMap<>();
    doc = getJsoup(url);
  }

  /**
   * Constructor.
   * @param url the entry-point html
   * @param env the environment with data which features are supported by your
   *     machine.
   * @param useFs use filesystem yes or no
   * @throws MalformedURLException when the url is malformed.
   */
  public Analyzer(String url, Env env, boolean useFs) throws MalformedURLException {
    if (useFs) {
      fileReader = new FileReaderLocal();
      this.url = url;
    } else {
      fileReader = new FileReaderWeb();
      URL parsedUrl = new URL(url);
      this.host = parsedUrl.getProtocol() + "://" + parsedUrl.getAuthority();
      this.url = parsedUrl.toString();
    }
    this.env = env;
    jsFrags = new ArrayList<>();
    tagStats = new HashMap<>();
    cssStats = new HashMap<>();
    doc = getJsoup(url);
  }

  /**
   * Gets a html document form a url or path.
   *
   * @param url the resource-location
   * @return a jsoup-document
   */
  public Document getJsoup(String url) {
    return fileReader.getHtml(url);
  }

  /**
   * Gets a js-file form an url or path.
   * @param url the url of the resource
   * @return String the js as a String
   */
  public String getJs(String url) {
    return fileReader.getFile(url);
  }

  /**
   * Collects all js-files and fragments and stores them in a Map.
   *
   * @throws IOException when a resource could not be read.
   */
  public void extractJsData() throws IOException {
    LOGGER.logInfo("getting scripts from -> " + url);

    Elements elements = doc.select("script");

    if (LOGGER.isInfo()) {
      LOGGER.logInfo(elements.size());
    }

    elements.forEach((element) -> {
      if (element.hasAttr("src")) {
        // external script-resource
        String src = element.attr("src");
        LOGGER.logVerbose("found src " + src);
        jsFrags.add(
                getJs(src)
        );
      } else {
        // inline script fragment
        String frag = element.html();
        LOGGER.logVerbose("found script-frag: " + frag);
        jsFrags.add(frag);
      }
    });
    LOGGER.logInfo("js-resources discovered -> " + jsFrags.size());
  }

  /**
   * Analyzes the body of a dom.
   */
  public void analyzeBody() {
    tagStats();
    cssStats();
  }

  /**
   * Counts different types of body tags and stores a count in a HashMap.
   *
   * @return HashMap the tag stats
   */
  public Map<String, Integer> tagStats() {
    // ? count different types of tags
    Element body = doc.selectFirst("body");
    Elements tags = body.getAllElements();
    int numTags = 0;
    for (Element tag : tags) {
      numTags += 1;
      String tagName = tag.tagName();
      LOGGER.logVerbose("found tag -> " + tagName);
      addToTagStats(tagName);
    }
    LOGGER.logInfo("total tags found        -> " + numTags);
    LOGGER.logInfo("unique tags found       -> " + tagStats.size());
    LOGGER.logInfo("tag-stats               -> " + tagStats);
    return tagStats;
  }

  /**
   * Increments the value of the key tag.
   *
   * @param tagName the tagName to add
   */
  private void addToTagStats(String tagName) {
    if (tagStats.containsKey(tagName)) {
      tagStats.put(tagName, tagStats.get(tagName) + 1);
    } else {
      tagStats.put(tagName, 1);
    }
  }

  /**
   * Counts all css-classes in the dom.
   *
   * @return Map the css-stats
   */
  public Map<String, Integer> cssStats() {
    Element body = doc.selectFirst("body");
    Elements tags = body.getAllElements();
    int numClasses = 0;
    for (Element tag : tags) {
      for (String className : tag.classNames()) {
        numClasses += 1;
        LOGGER.logVerbose("css-class found -> " + className);
        addToCssStats(className);
      }
    }
    LOGGER.logInfo("total classes found     -> " + numClasses);
    LOGGER.logInfo("unique classes found    -> " + cssStats.size());
    LOGGER.logInfo("cssStats                -> " + cssStats);
    return cssStats;
  }

  /**
   * Increments the value of the key css-class.
   *
   * @param tagName the tagName to add
   */
  void addToCssStats(String className) {
    if (cssStats.containsKey(className)) {
      cssStats.put(className, cssStats.get(className) + 1);
    } else {
      cssStats.put(className, 1);
    }
  }

}
