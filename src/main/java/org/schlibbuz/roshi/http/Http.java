/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.output.ByteArrayOutputStream;

/**
 * A really simple Http-Client.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class Http {

  private URL url;

  /**
   * Create a connection to the given url.
   * @param url the url to connect to
   * @throws MalformedURLException if the url is malformed
   */
  public Http(String url) throws MalformedURLException {
    this.url = new URL(url);
  }

  /**
   * Gets the contents of the url of this instance.
   * @return String or null in case of i/o error
   */
  public String get() {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    try (InputStream is = url.openStream()) {
      while ((length = is.read(buffer)) != -1) {
        result.write(buffer, 0, length);
      }
      return result.toString("UTF-8");
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
    return null;
  }

}
