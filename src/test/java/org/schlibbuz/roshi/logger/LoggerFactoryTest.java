/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.logger;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.ProcessRunner;

/**
 * Tests class LoggerFactory.
 * @author stefan.a.frei@gmail.com
 */
public class LoggerFactoryTest {

  /**
   * Default constructor with no arguments.
   */
  public LoggerFactoryTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of getInstance method, of class LoggerFactory.
   */
  @Test
  public void testGetInstance_Class() {
    System.out.println("getInstance");
    Class clazz = ProcessRunner.class;
    LoggerFactory instance = new LoggerFactory();
    Log result = instance.getInstance(clazz);
  }

  /**
   * Test of getInstance method, of class LoggerFactory.
   */
  @Test
  public void testGetInstance_String() {
    System.out.println("getInstance");
    String name = "ProcessRunner";
    LoggerFactory instance = new LoggerFactory();
    Log result = instance.getInstance(name);
  }

  /**
   * Test of getInstance method, of class LoggerFactory.
   */
  @Test
  public void testGetInstance_String_Existing() {
    System.out.println("getInstance exisiting");
    String name = "ProcessRunner";
    LoggerFactory instance = new LoggerFactory();
    Log result = instance.getInstance(name);
  }

}
