/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.logger;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests class Logger.
 * @author stefan.a.frei@gmail.com
 */
public class LoggerTest {

  private Logger logger;

  /**
   * Default constructor with no arguments.
   */
  public LoggerTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    logger = new Logger("test");
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of getLogLevel method, of class Logger.
   */
  @Test
  public void testGetLogLevel() {
    System.out.println("getLogLevel");

    LogLevel expResult = LogLevel.CRITICAL;
    logger.setLogLevel(LogLevel.CRITICAL);
    LogLevel result = logger.getLogLevel();
    assertEquals(expResult, result);

    expResult = LogLevel.ERROR;
    logger.setLogLevel(LogLevel.ERROR);
    result = logger.getLogLevel();
    assertEquals(expResult, result);

    expResult = LogLevel.WARNINGS;
    logger.setLogLevel(LogLevel.WARNINGS);
    result = logger.getLogLevel();
    assertEquals(expResult, result);

    expResult = LogLevel.INFO;
    logger.setLogLevel(LogLevel.INFO);
    result = logger.getLogLevel();
    assertEquals(expResult, result);

    expResult = LogLevel.VERBOSE;
    logger.setLogLevel(LogLevel.VERBOSE);
    result = logger.getLogLevel();
    assertEquals(expResult, result);
  }

  /**
   * Test of setLogLevel method, of class Logger.
   */
  @Test
  public void testSetLogLevel() {
    System.out.println("setLogLevel");
    LogLevel logLevel = LogLevel.CRITICAL;
    logger.setLogLevel(logLevel);
    assertEquals(LogLevel.CRITICAL, logger.getLogLevel());
  }

  /**
   * Test of isCritical method, of class Logger.
   */
  @Test
  public void testIsCritical() {
    System.out.println("isCritical");
    logger.setLogLevel(LogLevel.CRITICAL);

    boolean expResult = true;
    boolean result = logger.isCritical();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isError();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isWarn();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isInfo();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isVerbose();
    assertEquals(expResult, result);
  }

  /**
   * Test of isError method, of class Logger.
   */
  @Test
  public void testIsError() {
    System.out.println("isError");
    logger.setLogLevel(LogLevel.ERROR);

    boolean expResult = true;
    boolean result = logger.isCritical();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isError();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isWarn();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isInfo();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isVerbose();
    assertEquals(expResult, result);
  }

  /**
   * Test of isWarn method, of class Logger.
   */
  @Test
  public void testIsWarn() {
    System.out.println("isWarn");
    logger.setLogLevel(LogLevel.WARNINGS);

    boolean expResult = true;
    boolean result = logger.isCritical();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isError();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isWarn();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isInfo();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isVerbose();
    assertEquals(expResult, result);
  }

  /**
   * Test of isInfo method, of class Logger.
   */
  @Test
  public void testIsInfo() {
    System.out.println("isInfo");
    logger.setLogLevel(LogLevel.INFO);
    boolean expResult = true;
    boolean result = logger.isCritical();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isError();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isWarn();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isInfo();
    assertEquals(expResult, result);

    expResult = false;
    result = logger.isVerbose();
    assertEquals(expResult, result);
  }

  /**
   * Test of isVerbose method, of class Logger.
   */
  @Test
  public void testIsVerbose() {
    System.out.println("isVerbose");
    logger.setLogLevel(LogLevel.VERBOSE);

    boolean expResult = true;
    boolean result = logger.isCritical();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isError();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isWarn();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isInfo();
    assertEquals(expResult, result);

    expResult = true;
    result = logger.isVerbose();
    assertEquals(expResult, result);
  }

  /**
   * Test of logCritical method, of class Logger.
   */
  @Test
  public void testLogCritical_Object() {
    System.out.println("logCritical");
    Object msg = 5;
    logger.logCritical(msg);
  }

  /**
   * Test of logCritical method, of class Logger.
   */
  @Test
  public void testLogCritical_String() {
    System.out.println("logCritical");
    String msg = "blaa";
    logger.logCritical(msg);
  }

  /**
   * Test of logError method, of class Logger.
   */
  @Test
  public void testLogError_Object() {
    logger.setLogLevel(LogLevel.CRITICAL);
    System.out.println("logError");
    Object msg = 5;
    logger.logError(msg);
  }

  /**
   * Test of logError method, of class Logger.
   */
  @Test
  public void testLogError_String() {
    logger.setLogLevel(LogLevel.INFO);
    System.out.println("logError");
    String msg = "blaa";
    logger.logError(msg);
  }

  /**
   * Test of logWarn method, of class Logger.
   */
  @Test
  public void testLogWarn_Object() {
    System.out.println("logWarn");
    Object msg = 5;
    logger.logWarn(msg);
  }

  /**
   * Test of logWarn method, of class Logger.
   */
  @Test
  public void testLogWarn_String() {
    System.out.println("logWarn");
    String msg = "blaa";
    logger.logWarn(msg);
  }

  /**
   * Test of logInfo method, of class Logger.
   */
  @Test
  public void testLogInfo_Object() {
    logger.setLogLevel(LogLevel.CRITICAL);
    System.out.println("logInfo");
    Object msg = 5;
    logger.logInfo(msg);
  }

  /**
   * Test of logInfo method, of class Logger.
   */
  @Test
  public void testLogInfo_String() {
    logger.setLogLevel(LogLevel.VERBOSE);
    System.out.println("logInfo");
    String msg = "blaa";
    logger.logInfo(msg);
  }

  /**
   * Test of logVerbose method, of class Logger.
   */
  @Test
  public void testLogVerbose_Object() {
    logger.setLogLevel(LogLevel.INFO);
    System.out.println("logVerbose");
    Object msg = 5;
    logger.logVerbose(msg);
  }

  /**
   * Test of logVerbose method, of class Logger.
   */
  @Test
  public void testLogVerbose_String() {
    logger.setLogLevel(LogLevel.VERBOSE);
    System.out.println("logVerbose");
    String msg = "blaa";
    logger.logVerbose(msg);
  }

  /**
   * Test of hashCode method, of class Logger.
   */
  @Test
  public void testHashCode() {
    System.out.println("hashCode");
    int expResult = 3556863;
    int result = logger.hashCode();
    assertEquals(expResult, result);
  }

  /**
   * Test of equals method, of class Logger.
   */
  @Test
  public void testEquals_Object_Same() {
    System.out.println("equals");
    boolean expResult = true;
    boolean result = logger.equals(logger);
    assertEquals(expResult, result);
  }

  /**
   * Test of equals method, of class Logger.
   */
  @Test
  public void testEquals_Object_Similar() {
    System.out.println("equals");
    boolean expResult = true;
    boolean result = logger.equals(new Logger("test"));
    assertEquals(expResult, result);
  }

  /**
   * Test of equals method, of class Logger.
   */
  @Test
  public void testEquals_Object_Other() {
    System.out.println("equals");
    boolean expResult = false;
    boolean result = logger.equals(new Logger("test2"));
    assertEquals(expResult, result);
  }

  /**
   * Test of equals method, of class Logger.
   */
  @Test
  public void testEquals_Object_Other_Class() {
    System.out.println("equals");
    boolean expResult = false;
    boolean result = logger.equals(new String());
    assertEquals(expResult, result);
  }

  /**
   * Test of equals method, of class Logger.
   */
  @Test
  public void testEquals_Null() {
    System.out.println("equals");
    boolean expResult = false;
    boolean result = logger.equals(null);
    assertEquals(expResult, result);
  }

}
