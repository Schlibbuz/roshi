/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.Env;

/**
 * Tests GenericFeature.
 * @author stefan
 */
public class GenericFeatureTest {

  Env env;

  /**
   * Default constructor with no arguments.
   */
  public GenericFeatureTest() {
    env = new Env();
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Tests construction of a GenericFeature.
   */
  @Test
  public void testConstructor() {
    Feature instance = new GenericFeature("bash", env);
    assertNotNull(instance);
  }

  /**
   * Tests getSystemBinaries.
   */
  @Test
  public void testGetSystemBinaries() {
    var instance = new GenericFeature("bash", env);
    var sysBins = instance.getSystemBinaries();
    assertTrue(sysBins instanceof java.util.Map);
  }

}
