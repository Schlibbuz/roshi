/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.Env;


/**
 * Tests class NodeFeature.
 * @author stefan.a.frei@gmail.com
 */
public class NodeFeatureTest {

  Env env;

  /**
   * Default constructor with no arguments.
   */
  public NodeFeatureTest() {
    env = new Env();
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of find method, of class NodeFeature.
   */
  @Test
  public void testFind() {
    System.out.println("find");
    NodeFeature instance = new NodeFeature(env);
    instance.find();
    assertTrue(instance instanceof org.schlibbuz.roshi.feature.NodeFeature);
    var systemBinaries = instance.systemBinaries;
    assertTrue(systemBinaries.size() == 1);
    var systemBinary = systemBinaries.get("node");
    assertEquals("node", systemBinary.getName());
    assertThat(systemBinary.getVersion(), matchesPattern("^[\\d\\.]+$"));
  }

}
