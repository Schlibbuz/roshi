/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.Env;

/**
 * Tests FeatureFactory.
 * @author stefan.a.frei@gmail.com
 */
public class FeatureFactoryTest {

  Env env;
  
  /**
   * Default constructor with no arguments.
   */
  public FeatureFactoryTest() {
    env = new Env();
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of constructor of class FeatureFactory.
   */
  @Test
  public void testConstruct() {
    var instance = new FeatureFactory();
    assertTrue(instance instanceof FeatureFactory);
  }

  /**
   * Test of produce method, of class FeatureFactory.
   * @throws org.schlibbuz.roshi.feature.UnsupportedFeatureException
   *     if feature is not supported
   */
  @Test
  public void testProduce_Node() throws UnsupportedFeatureException {
    System.out.println("produce node");
    String name = "node";
    Feature result = FeatureFactory.produce(name, env);
    assertTrue(result instanceof NodeFeature);
  }

  /**
   * Test of produce method, of class FeatureFactory.
   * @throws org.schlibbuz.roshi.feature.UnsupportedFeatureException
   *     if feature is not supported
   */
  @Test
  public void testProduce_Containerization() throws UnsupportedFeatureException {
    System.out.println("produce containerization");
    String name = "containerization";
    Feature result = FeatureFactory.produce(name, env);
    assertTrue(result instanceof ContainerizationFeature);
  }

  /**
   * Test of produce method, of class FeatureFactory.
   * @throws org.schlibbuz.roshi.feature.UnsupportedFeatureException
   *     if feature is not supported
   */
  @Test
  public void testProduce_Selenium() throws UnsupportedFeatureException {
    System.out.println("produce selenium");
    String name = "selenium";
    Feature expResult = null;
    Feature result = FeatureFactory.produce(name, env);
    assertTrue(result instanceof SeleniumFeature);
  }

  /**
   * Test of produce method, of class FeatureFactory.
   * @throws org.schlibbuz.roshi.feature.UnsupportedFeatureException
   *     if feature is not supported
   */
  @Test
  public void testProduce_Exception() throws UnsupportedFeatureException {
    System.out.println("produce krokkodillmandrillfant");
    String name = "krokkodillmandrillfant";
    assertThrows(UnsupportedFeatureException.class, () -> {
      FeatureFactory.produce(name, env);
    });
  }

}
