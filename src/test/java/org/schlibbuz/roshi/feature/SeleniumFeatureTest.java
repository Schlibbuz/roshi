/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.Env;
import org.schlibbuz.roshi.SystemBinary;

/**
 * Tests class SeleniumFeature.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class SeleniumFeatureTest {

  Env env;
  SeleniumFeature instance;

  /**
   * Default constructor with no arguments.
   */
  public SeleniumFeatureTest() {
    env = new Env();
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    instance = new SeleniumFeature(env);
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Tests method verifyCompatibility.
   * @throws IOException in case of i/o error
   */
  @Test
  public void testVerifyCompatibility_NoDriver() throws IOException {
    boolean expected = false;
    boolean actual = instance.verifyCompatibility();
    assertEquals(expected, actual);
  }

  /**
   * Tests method verifyCompatibility.
   * @throws IOException in case of i/o error
   */
  @Test
  public void testVerifyCompatibility_No_Chrome() throws IOException {
    boolean expected = false;
    instance.systemBinaries.put("chromedriver", new SystemBinary("chromedriver"));
    boolean actual = instance.verifyCompatibility();
    assertEquals(expected, actual);
  }

  /**
   * Tests method verifyCompatibility.
   * @throws IOException in case of i/o error
   */
  @Test
  public void testVerifyCompatibility_Chromium_And_Driver_Avail() throws IOException {
    boolean expected = true;
    instance.systemBinaries.put("chromedriver", new SystemBinary("chromedriver"));
    instance.systemBinaries.put("chromium-browser", new SystemBinary("chromium-browser"));
    boolean actual = instance.verifyCompatibility();
    assertEquals(expected, actual);
  }

  /**
   * Tests method getBrowserVersionToQuery.
   */
  @Test
  public void testGetBrowserVersionToQuery() {
    String expected = "78.0.3904";
    String actual = instance.getBrowserVersionToQuery("78.0.3904.99");
    assertEquals(expected, actual);
  }

  /**
   * Tests method requestMatchingDriverVersion for 78.
   */
  @Test
  public void testRequestMatchingDriverVersion78() {
    String exp = "78.0.3904.70";
    String act = instance.requestMatchingDriverVersion(
            "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_78.0.3904"
    );
    assertEquals(exp, act);
  }

  /**
   * Tests method requestMatchingDriverVersion for 77.
   */
  @Test
  public void testRequestMatchingDriverVersion77() {
    String exp = "77.0.3865.40";
    String act = instance.requestMatchingDriverVersion(
            "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_77.0.3865"
    );
    assertEquals(exp, act);
  }

  /**
   * Tests method requestMatchingDriverVersion, fail case.
   */
  @Test
  public void testRequestMatchingDriverVersion_Fail() {
    String exp = null;
    String act = instance.requestMatchingDriverVersion(
            "httpsdsfds://chromedriver.storage.googleapis.com/LATEST_RELEASE_77.0.3865"
    );
    assertEquals(exp, act);
  }

  /**
   * Tests composeChromiumDriverApiUrl method, of class SelenuimFeature.
   */
  @Test
  public void testComposeChromiumDriverApiUrl() {
    String expected = "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_78.0.3904";
    String actual = instance.composeChromiumDriverApiUrl("78.0.3904.97");
    assertEquals(expected, actual);
  }

}
