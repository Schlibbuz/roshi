/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.feature;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Tests UnsupportedFeatureException.
 * @author stefan.a.frei@gmail.com
 */
public class UnsupportedFeatureExceptionTest {

  @Test
  public void testConstruct() {
    var inst = new UnsupportedFeatureException();
    assertTrue(inst instanceof UnsupportedFeatureException);
  }

}
