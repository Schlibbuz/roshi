/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.feature.FeatureFactory;
import org.schlibbuz.roshi.feature.UnsupportedFeatureException;
import org.schlibbuz.roshi.logger.Log;
import org.schlibbuz.roshi.logger.LogLevel;
import org.schlibbuz.roshi.logger.LoggerFactory;

/**
 * Test of class Analyzer.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class AnalyzerTest {

  private static final Log LOGGER = new LoggerFactory().getInstance(AnalyzerTest.class);
  private static Env env;
  private Analyzer instance;

  /**
   * Default constructor with no arguemnts.
   */
  public AnalyzerTest() {
  }

  /**
   * Runs once before all test.
   * @throws UnsupportedFeatureException if a feature is not supported
   */
  @BeforeAll
  public static void setUpClass() throws UnsupportedFeatureException {
    env = new Env();
    env.addFeature(FeatureFactory.produce("node", env));
    env.addFeature(FeatureFactory.produce("selenium", env));
    env.addFeature(FeatureFactory.produce("containerization", env));
  }

  /**
   * Runs once after all tests. Used for cleanup.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   * @throws MalformedURLException if the url is malformed
   */
  @BeforeEach
  public void setUp() throws MalformedURLException {
    instance = new Analyzer("data/test/1/index.html", new Env(), true);

  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test construct, of class Analyzer.
   * @throws java.net.MalformedURLException if urtl is malformed
   */
  @Test
  public void testAnalyzer() throws MalformedURLException {
    instance = new Analyzer("http://www.myip.ch/", new Env());
    assertTrue(instance instanceof Analyzer);
  }

  /**
   * Test of getJsoup method, of class Analyzer.
   */
  @Test
  public void testGetJsoup_Local() {
    System.out.println("getJsoup");
    Document result = instance.getJsoup("data/test/1/index.html");
    assertTrue(result instanceof org.jsoup.nodes.Document);
  }

  /**
   * Test of getJsoup method, of class Analyzer.
   */
  @Test
  public void testGetJsoup_Web() throws MalformedURLException {
    System.out.println("getJsoup");
    instance = new Analyzer("http://www.myip.ch/", env, false);
    Document result = instance.getJsoup("http://www.myip.ch/");
    assertTrue(result instanceof org.jsoup.nodes.Document);
  }

  /**
   * Test of getJsoup method, of class Analyzer.
   */
  @Test
  public void testGetJsoup_Except_Malformed() {
    System.out.println("getJsoup");
    assertThrows(MalformedURLException.class, () -> {
      instance = new Analyzer("httpxyxc://www.myip.ch/", env, false);
    });
  }

  /**
   * Test of getJs method, of class Analyzer.
   */
  @Test
  public void testGetJs() {
    System.out.println("getJs");
    String result = instance.getJs("data/test/1/js/site.js");
    assertTrue(result.contains("console.log"));
  }

  /**
   * Test of extractJsData method, of class Analyzer.
   */
  @Test
  public void testExtractJsDataLocal() {
    LOGGER.setLogLevel(LogLevel.CRITICAL);
    System.out.println("extractJsData");
    try {
      instance = new Analyzer("data/test/1/index.html", env, true);
      instance.extractJsData();
    } catch (IOException ex) {
      Logger.getLogger(AnalyzerTest.class.getName()).log(Level.SEVERE, null, ex);
    }

  }

  /**
   * Test of extractJsData method, of class Analyzer.
   */
  @Test
  public void testExtractJsDataLocal_Log_Info() {
    System.out.println("extractJsData");
    LOGGER.setLogLevel(LogLevel.VERBOSE);
    try {
      instance = new Analyzer("data/test/1/index.html", env, true);
      instance.extractJsData();
    } catch (IOException ex) {
      Logger.getLogger(AnalyzerTest.class.getName()).log(Level.SEVERE, null, ex);
    }

  }

  /**
   * Test of tagStats method, of class Analyzer.
   */
  @Test
  public void testTagStats() {
    System.out.println("analyzeBody");
    Map<String, Integer> result = instance.tagStats();
    assertTrue(result instanceof java.util.HashMap);
    assertThat("tagStats-Size", result.size(), greaterThan(0));
  }

  /**
   * Test of cssStats method, of class Analyzer.
   */
  @Test
  public void testCssStats() {
    System.out.println("cssStats");
    Map<String, Integer> result = instance.cssStats();
    assertTrue(result instanceof java.util.HashMap);
    assertThat("cssStats-Size", result.size(), greaterThan(0));
  }

  /**
   * Test of cssStats method, of class Analyzer.
   */
  @Test
  public void testAddToCssStats() {
    System.out.println("addToCssStats");
    instance.addToCssStats("blaa");
    instance.addToCssStats("blaa");
  }

}
