/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.schlibbuz.roshi.feature.FeatureFactory;
import org.schlibbuz.roshi.feature.UnsupportedFeatureException;

/**
 * Tests class Env.
 * @author stefan
 */
public class EnvTest {


  private Env env;

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    env = new Env();
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of getOs method, of class Env.
   */
  @Test
  public void testGetOs() {
    System.out.println("getOs");
    Env instance = new Env();
    String expResult = "Linux";
    String result = instance.getOs();
    assertEquals(expResult, result);
  }

  /**
   * Test of addFeature method, of class Env.
   * @throws org.schlibbuz.roshi.feature.UnsupportedFeatureException if feature is not supported
   */
  @Test
  public void testAddFeature() throws UnsupportedFeatureException {
    System.out.println("addFeature");
    var feature = FeatureFactory.produce("node", env);
    env.addFeature(feature);
    var actual = env.getFeature("node");
    assertEquals(feature, actual);
  }

  /**
   * Test of addSystemBinary method, of class Env.
   */
  @Test
  public void testAddSystemBinary() {
    System.out.println("addSystemBinary");
    SystemBinary binary = new SystemBinary("node");
    env.addSystemBinary(binary);
    assertEquals(binary, env.getSystemBinary("node"));
  }

  /**
   * Test of supports method, of class Env.
   * @throws org.schlibbuz.roshi.feature.UnsupportedFeatureException if feature is not supported.
   */
  @Test
  public void testSupports() throws UnsupportedFeatureException {
    System.out.println("supports");
    boolean expResult = true;
    env.addFeature(FeatureFactory.produce("node", env));
    boolean result = env.supports("node");
    assertEquals(expResult, result);
  }

  /**
   * Test of supports method, of class Env.
   */
  @Test
  public void testSupports_Fail() {
    System.out.println("supports");
    boolean expResult = false;
    boolean result = env.supports("krokkodillpfau");
    assertEquals(expResult, result);
  }

  /**
   * Test of toString method, of class Env.
   */
  @Test
  public void testToString() {
    System.out.println("toString");
    System.out.println(env);
  }

}
