/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests class ProcessRunner.
 * @author stefan.a.frei@gmail.com
 */
public class ProcessRunnerTest {

  private ProcessRunner instance;

  /**
   * Default constructor with no arguments.
   */
  public ProcessRunnerTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    instance = new ProcessRunner();
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of run method, of class ProcessRunner.
   *
   * @throws java.lang.InterruptedException if the process was interrupted
   * @throws java.io.IOException in case of i/o error
   */
  @Test
  public void testRun() throws InterruptedException, IOException {
    System.out.println("run");
    String[] commands = {"ls", "-lisa"};
    if (System.getProperty("os.name").equals("Windows")) {
      commands = new String[]{"dir"};
    }
    Process result = instance.run(commands);
    result.waitFor();
    assertEquals(0, result.exitValue());
  }

}
