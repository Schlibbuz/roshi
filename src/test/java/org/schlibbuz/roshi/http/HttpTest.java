/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.http;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.net.MalformedURLException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests for class Http.
 * @author stefan
 */
public class HttpTest {

  /**
   * Default constructor with no arguments.
   */
  public HttpTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test construct, of class Http.
   */
  @Test
  public void testHttp_Except_Malformed() {
    assertThrows(MalformedURLException.class, () -> {
      new Http("htsdjfh://localhost:50000/");
    });
  }
  
  /**
   * Test of get method, of class Http.
   * @throws java.io.IOException in case of i/o error
   */
  @Test
  public void testGet() throws IOException {
    System.out.println("get");
    Http instance = new Http("https://chromedriver.storage.googleapis.com/LATEST_RELEASE_78.0.3904");
    String expResult = "78.0.3904.70";
    String result = instance.get();
    assertEquals(expResult, result);
  }

  /**
   * Test of get method, of class Http.
   * @throws java.net.MalformedURLException if the url is malformed
   */
  @Test
  public void testGet_IO_Except() throws MalformedURLException {
    System.out.println("get");
    String expected = null;
    String actual = new Http("http://localhost:34222/").get();
    assertEquals(expected, actual);
  }

}
