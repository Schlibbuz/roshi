/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * Tests class FileReaderLocal.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class FileReaderLocalTest {

  private FileReaderLocal instance;

  /**
   * Default constructor with no arguments.
   */
  public FileReaderLocalTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    instance = new FileReaderLocal();
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of getHtml method, of class FileReaderLocal.
   */
  @Test
  public void testGetHtml() {
    System.out.println("getHtml");
    String expResult = "TODO supply a title";
    Document result = instance.getHtml("data/test/1/index.html");
    assertEquals(expResult, result.title());
  }

  /**
   * Test of getHtml method, of class FileReaderLocal.
   */
  @Test
  public void testGetHtml_Invalid_Path() {
    System.out.println("getHtml_throws");
    Document result = instance.getHtml("not/a/valid/path");
    assertNull(result);
  }

  /**
   * Test of getFile method, of class FileReaderLocal.
   */
  @Test
  public void testGetFile() {
    System.out.println("getFile");
    String expResult = "html {";
    String result = instance.getFile("data/test/1/css/site.css");
    assertTrue(result.contains(expResult));
  }

  /**
   * Test of getFile method, of class FileReaderLocal.
   */
  @Test
  public void testGetFile_Invalid_Path() {
    System.out.println("getFile");
    String result = instance.getFile("not/a/valid/path");
    assertNull(result);
  }

}
