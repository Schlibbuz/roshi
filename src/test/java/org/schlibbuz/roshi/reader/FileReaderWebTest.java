/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi.reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * Tests class FileReaderWeb.
 * @author Stefan Frei stefan.a.frei@gmail.com
 */
public class FileReaderWebTest {

  private FileReaderWeb instance;

  /**
   * Default constructor with no arguments.
   */
  public FileReaderWebTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    instance = new FileReaderWeb();
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of getHtml method, of class FileReaderWeb.
   */
  @Test
  public void testGetHtml() {
    System.out.println("getHtml");
    String expResult = "Current IP Check";
    Document result = instance.getHtml("http://www.myip.ch/");
    assertEquals(expResult, result.title());
  }

  /**
   * Test of getFile method, of class FileReaderWeb.
   */
  @Test
  public void testGetFile() {
    System.out.println("getFile");
    String expResult = "<title>Current IP Check</title>";
    String result = instance.getFile("http://www.myip.ch/");
    assertTrue(result.contains(expResult));
  }

  /**
   * Test retry-logic for min iterations.
   */
  @Test
  public void testRetryXTimes_MinPass() {
    String exp = "blaa";
    final int maxRetries = 5;
    String act = instance.retryXTimes(() -> {
      return "blaa";
    }, maxRetries);
    assertEquals(exp, act);
  }

  /**
   * Test retry-logic for half iterations.
   */
  @Test
  public void testRetryXTimes_HalfPass() {
    String exp = "blaa";
    final int maxRetries = 5;
    final List<String> retryCount = new ArrayList<>();
    String act = instance.retryXTimes(() -> {
      retryCount.add("retry");
      if (retryCount.size() < maxRetries / 2) {
        throw new IOException();
      }
      return "blaa";
    }, maxRetries);
    assertEquals(exp, act);
  }

  /**
   * Test retry-logic for max iterations.
   */
  @Test
  public void testRetryXTimes_MaxPass() {
    String exp = "blaa";
    final int maxRetries = 5;
    final List<String> retryCount = new ArrayList<>();
    String act = instance.retryXTimes(() -> {
      retryCount.add("retry");
      if (retryCount.size() < maxRetries) {
        throw new IOException();
      }
      return "blaa";
    }, maxRetries);
    assertEquals(exp, act);
  }

  /**
   * Test retry-logic fail-case.
   */
  @Test
  public void testRetryXTimes_Fail() {
    String exp = null;
    final int maxRetries = 5;
    final List<String> retryCount = new ArrayList<>();
    String act = instance.retryXTimes(() -> {
      retryCount.add("retry");
      if (retryCount.size() < maxRetries + 1) {
        throw new IOException();
      }
      return "blaa";
    }, maxRetries);
    assertEquals(exp, act);
  }

}
