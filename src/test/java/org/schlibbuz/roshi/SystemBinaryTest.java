/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * Tests class SystemBinary.
 * @author stefan.a.frei@gmail.com
 */
public class SystemBinaryTest {

  /**
   * Default constructor with no arguments.
   */
  public SystemBinaryTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Tests constructor of class SystemBinary.
   */
  @Test
  public void testSystemBinary() {
    var instance = new SystemBinary("bash");
    assertTrue(instance instanceof SystemBinary);
  }

  /**
   * Test of getName method, of class SystemBinary.
   */
  @Test
  public void testGetName() {
    System.out.println("getName");
    SystemBinary instance = new SystemBinary("krokodil");
    String expResult = "krokodil";
    String result = instance.getName();
    assertEquals(expResult, result);
  }

  /**
   * Test of getVersion method, of class SystemBinary.
   */
  @Test
  public void testGetVersion() {
    System.out.println("getVersion");
    SystemBinary instance = null;
    if (System.getProperty("os.name").equals("Windows")) {
      instance = new SystemBinary("cmd");
    } else {
      instance = new SystemBinary("bash");
    }
    // best we can do
    assertTrue(instance.getVersion().matches("^[\\d\\.]*$"));
  }

  /**
   * Test of cleanVersion method, of class SystemBinary.
   */
  @Test
  public void testCleanVersion() {
    var instance = new SystemBinary("bash");
    String exp = "4.4.20";
    String act = instance.cleanVersion("GNU bash, version 4.4.20(1)-release (x86_64-pc-linux-gnu)");
    assertEquals(exp, act);
  }

  /**
   * Test of cleanVersion method, of class SystemBinary.
   */
  @Test
  public void testCleanVersion_Null() {
    var instance = new SystemBinary("bash");
    String exp = null;
    String act = instance.cleanVersion(null);
    assertEquals(exp, act);
  }

  /**
   * Test of cleanVersion method, of class SystemBinary.
   */
  @Test
  public void testCleanVersion_NoMatch() {
    var instance = new SystemBinary("bash");
    String exp = null;
    var act = instance.cleanVersion(("abc"));
    assertEquals(exp, act);
  }

}
