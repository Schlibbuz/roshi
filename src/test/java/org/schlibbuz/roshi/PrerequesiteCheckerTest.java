/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.schlibbuz.roshi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests for class PrerequesiteChecker.
 * @author stefan
 */
public class PrerequesiteCheckerTest {

  private PrerequesiteChecker instance;

  /**
   * Default constructor with no arguments.
   */
  public PrerequesiteCheckerTest() {
  }

  /**
   * Runs once before all tests.
   */
  @BeforeAll
  public static void setUpClass() {
  }

  /**
   * Runs once after all tests.
   */
  @AfterAll
  public static void tearDownClass() {
  }

  /**
   * Runs before each test.
   */
  @BeforeEach
  public void setUp() {
    instance = new PrerequesiteChecker();
  }

  /**
   * Runs after each test.
   */
  @AfterEach
  public void tearDown() {
  }

  /**
   * Test of checkSys method, of class PrerequesiteChecker.
   *
   * @throws java.lang.Exception when an unsupported feature is requested
   */
  @Test
  public void testCheckSys() throws Exception {
    System.out.println("checkSys");
    Env result = instance.checkSys();
    assertTrue(result instanceof org.schlibbuz.roshi.Env);
  }

  /**
   * Test of checkSysDeps method, of class PrerequesiteChecker.
   *
   * @throws java.lang.Exception when an unsupported feature is requested
   */
  @Test
  public void testCheckSysDeps() throws Exception {
    System.out.println("checkSysDeps");
    instance.checkSysDeps();
  }

  /**
   * Test of checkOs method, of class PrerequesiteChecker.
   */
  @Test
  public void testCheckOs() {
    System.out.println("checkOs");
    instance.checkOs();
    Env env = instance.getEnv();
    assertEquals(System.getProperty("os.name"), env.getOs());
  }

  /**
   * Test of checkFeature method, of class PrerequesiteChecker.
   */
  @Test
  public void testCheckFeature() {
    System.out.println("checkFeature");
    Env env = instance.getEnv();

    instance.checkFeature("node");
    instance.checkFeature("selenium");
    instance.checkFeature("containerization");
  }

  /**
   * Test of checkFeature method, of class PrerequesiteChecker.
   */
  @Test
  public void testCheckFeature_Except() {
    System.out.println("checkFeature");
    Env env = instance.getEnv();

    instance.checkFeature("krokkodill");
  }

}
